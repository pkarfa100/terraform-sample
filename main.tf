provider "aws" {
  region = "${var.aws_region}"
  alias  = "sandbox1"
}

#terraform {
#  backend "s3" {
#    bucket = "la-terraform-course-state"
#    key = "terraform/terraform.tfstate"
#    region = "us-west-2"
#  }
#}

# Deploy Storage Resource
module "storage" {
  source       = "./storage"
  project_name = "${var.project_name}"
}

# Deploy Networking Resources

module "networking" {
  project_name = "${var.project_name}"
  source       = "./networking"
  vpc_cidr     = "${var.vpc_cidr}"
  public_cidrs = "${var.public_cidrs}"
  accessip     = "${var.accessip}"
}

# Deploy Compute Resources

module "compute" {
  project_name     = "${var.project_name}"
  source           = "./compute"
  instance_count   = "${var.instance_count}"
  key_name         = "${var.key_name}"
  public_key_path  = "${var.public_key_path}"
  instance_type    = "${var.server_instance_type}"
  subnets          = "${module.networking.public_subnets}"
  security_group   = "${module.networking.public_sg}"      # tf_public_sg.id from Netowork module output
  subnet_ips       = "${module.networking.subnet_ips}"     # subnet_ips from Netowork module output
  private_key_path = "${var.private_key_path}"
}
