aws_region = "ap-southeast-1"

project_name = "prod-terri-terraform"

vpc_cidr = "10.30.0.0/16"

public_cidrs = [
  "10.30.1.0/24",
  "10.30.2.0/24",
]

accessip = "0.0.0.0/0"

key_name = "tf_key"

public_key_path = "/home/ec2-user/.ssh/id_rsa.pub"

server_instance_type = "t2.micro"

instance_count = 2

private_key_path = "/home/ec2-user/environment/mykey.pem"
