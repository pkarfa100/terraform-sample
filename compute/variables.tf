#-----compute/variables.tf

variable "project_name" {}

variable "key_name" {}

variable "public_key_path" {}

variable "private_key_path" {}

variable "subnet_ips" {
  type = "list"
}

variable "instance_count" {}

variable "instance_type" {}

variable "security_group" {}

variable "subnets" {
  type = "list" # This will passed from the root main.tf file as an output of network module
}
