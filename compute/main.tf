#-----compute/main.tf

data "aws_ami" "server_ami" {
  most_recent = true

  filter {
    name   = "owner-alias"
    values = ["amazon"]
  }

  filter {
    name   = "name"
    values = ["amzn-ami-hvm*-x86_64-gp2"]
  }
}

resource "aws_key_pair" "tf_auth" {
  key_name   = "${var.project_name}_${var.key_name}"
  public_key = "${file(var.public_key_path)}"
}

data "template_file" "user-init" {
  count    = 2
  template = "${file("${path.module}/userdata.tpl")}"

  vars {
    firewall_subnets = "${element(var.subnet_ips, count.index)}"
  }
}

resource "aws_instance" "tf_server" {
  count         = "${var.instance_count}"
  instance_type = "${var.instance_type}"
  ami           = "${data.aws_ami.server_ami.id}"

  tags {
    Name = "${var.project_name}_tf_server-${count.index +1}"
  }

  key_name               = "${aws_key_pair.tf_auth.id}"
  vpc_security_group_ids = ["${var.security_group}"]
  subnet_id              = "${element(var.subnets, count.index)}"
  user_data              = "${data.template_file.user-init.*.rendered[count.index]}"
}

resource "null_resource" "copy-script-file" { # this need to have a count for number of instances 
  connection {
    type        = "ssh"
    host        = "${aws_instance.tf_server.public_ip}"
    user        = "ec2-user"
    private_key = "${file(var.private_key_path)}"
  }

  # Copies the folder using SSH
  provisioner "file" {
    source      = "./Ansible-code/workspace/"  #--copies the playbooks, this need to be parametrized
    destination = "/home/ec2-user"
  }

  # Copies the file using SSH  
  provisioner "file" {
    source      = "./compute/script.sh"  #--a script to be executed once the instances boot up
    destination = "/home/ec2-user/script.sh"
  }

  # Exceute the script on remote machine
  provisioner "remote-exec" {
    inline = [
      "chmod +x /home/ec2-user/script.sh",
      "./script.sh", #-- execute the script
    ]
  }
}
