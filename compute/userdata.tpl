#!/bin/bash
yum-config-manager --enable epel
yum update repolist
yum update -y
yum install ansible -y
yum install httpd -y
echo "Subnet for Firewall: ${firewall_subnets}" >> /var/www/html/index.html
service httpd start
chkconfig httpd on
