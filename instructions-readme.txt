$ terraform workspace list // The command will list all existing workspaces
$ terraform workspace new <workspace_name> // The command will create a workspace
$ terraform workspace select <workspace_name> // The command select a workspace
$ terraform workspace delete <workspace_name> // The command delete a workspace

terraform plan -var-file=dev.tfvars -out=dev.plan.out //Not working now

terraform plan -var-file=dev.tfvars 

terraform apply -var-file=dev.tfvars  

terraform destroy -var-file=dev.tfvars

#----- connecting to ec2 instance
ssh -i /home/ec2-user/environment/mykey.pem ec2-user@13.229.104.108