#------networking/variables.tf
variable "project_name" {}

variable "vpc_cidr" {}

variable "public_cidrs" {
  type = "list"
}

variable "accessip" {}
